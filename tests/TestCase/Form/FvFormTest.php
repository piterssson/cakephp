<?php
namespace App\Test\TestCase\Form;

use App\Form\FvForm;
use Cake\TestSuite\TestCase;

/**
 * App\Form\FvForm Test Case
 */
class FvFormTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Form\FvForm
     */
    public $Fv;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Fv = new FvForm();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Fv);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
