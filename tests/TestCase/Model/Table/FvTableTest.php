<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FvTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FvTable Test Case
 */
class FvTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FvTable
     */
    public $Fv;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fv'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Fv') ? [] : ['className' => FvTable::class];
        $this->Fv = TableRegistry::get('Fv', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Fv);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
