<table id="example" class="display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>id</th>
        <th>Numer fv</th>
        <th>Utworzono</th>
        <th>Wartość</th>
        <th>Firma</th>
        <th>Adres</th>
        <th>Email</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>id</th>
        <th>Numer fv</th>
        <th>Utworzono</th>
        <th>Wartość</th>
        <th>Firma</th>
        <th>Adres</th>
        <th>Email</th>
    </tr>
    </tfoot>
</table>
<?php $url = $this->Url->build('/admin/fv/get', true) ?>
<?php $this->start('block') ?>
<script>
    $(document).ready(function() {
        $('#example').DataTable( {
            "ajax": '<?= $url ?>',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'fvNumber', name: 'fvNumber' },
                { data: 'created', name: 'created' },
                { data: 'value', name: 'value' },
                { data: 'company', name: 'company' },
                { data: 'address', name: 'address' },
                { data: 'email', name: 'email' },
            ],
        } );
    } );
</script>
<?php $this->end() ?>
