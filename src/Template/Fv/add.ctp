<?= $this->Form->create($fv); ?>

<div class="col-sm-6">
    <div class="form-group">
        <label for="">Fv numer</label>
        <?= $this->Form->field('fvNumber', ['class'=>'form-control']); ?>
    </div>

    <div class="form-group">
        <label for="">Wartość</label>
        <?= $this->Form->field('value', ['class'=>'form-control']); ?>
    </div>

    <div class="form-group">
        <label for="">Firma</label>
        <?= $this->Form->field('company', ['class'=>'form-control']); ?>
    </div>

    <div class="form-group">
        <label for="">Address</label>
        <?= $this->Form->field('address', ['class'=>'form-control']); ?>
    </div>

    <div class="form-group">
        <label for="">Email</label>
        <?= $this->Form->field('email', ['class'=>'form-control']); ?>
    </div>

    <?= $this->Form->button('Wyslij', array('class' => 'btn btn-success')); ?>

<?= $this->Form->end(); ?>

</div>
