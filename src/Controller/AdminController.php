<?php
/**
 *  APP
 */
namespace App\Controller;

/**
 * Class AdminController
 * @package App\Controller
 */
class AdminController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function index()
    {
        $this->viewBuilder()->setLayout('admin');
    }

    public function settings()
    {
        $this->viewBuilder()->setLayout('admin');
    }
}
