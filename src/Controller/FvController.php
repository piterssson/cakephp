<?php

namespace App\Controller;

use App\Controller\AppController;
use App\Form\FvForm;

/**
 * Fv Controller
 *
 * @property \App\Model\Table\FvTable $Fv
 *
 * @method \App\Model\Entity\Fv[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FvController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('admin');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $fv = $this->paginate($this->Fv);
        $this->set(compact('fv'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fvForm = new FvForm();
        $fvEntity = $this->Fv->newEntity();
        if ($this->request->is('post')) {
            if ($fvForm->execute($this->request->getData())) {
                $fv = $this->Fv->newEntity($this->request->getData());
                $this->Fv->save($fv);
                $this->Flash->success('We will get back to you soon.');
            } else {
                $this->Flash->error('There was a problem submitting your form.');
            }
        }
        $this->set('fv', $fvForm);
    }

    /**
     * @return \Cake\Http\Response|null
     */
    public function get()
    {
        $this->autoRender = false;
        $data = json_encode(array('data'=>$this->Fv->find('all')));
        $this->response->type('json');
        $this->response->body($data);
        return $this->response;
    }
}
