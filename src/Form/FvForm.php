<?php

namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

/**
 * Fv Form.
 */
class FvForm extends Form
{
    /**
     * Builds the schema for the modelless form
     *
     * @param \Cake\Form\Schema $schema From schema
     * @return \Cake\Form\Schema
     */
    protected function _buildSchema(Schema $schema)
    {
        return $schema->addField('fvNumber', 'string')
            ->addField('created', ['type' => 'string'])
            ->addField('value', ['type' => 'text'])
            ->addField('company', ['type' => 'text'])
            ->addField('address', ['type' => 'text'])
            ->addField('email', ['type' => 'text']);
    }

    /**
     * Form validation builder
     *
     * @param \Cake\Validation\Validator $validator to use against the form
     * @return \Cake\Validation\Validator
     */
    protected function _buildValidator(Validator $validator)
    {
        return $validator->add('email', 'format', [
            'rule' => 'email',
            'message' => 'A valid email address is required',
            ])
            ->notEmpty('fvNumber', 'Please fill this field')
            ->notEmpty('value', 'Please fill this field')
            ->notEmpty('company', 'Please fill this field')
            ->notEmpty('address', 'Please fill this field')
            ->add('value', [
                'numeric' => array(
                    'rule' => 'numeric',
                    'message' => 'Numbers only'
                )
            ]);
    }

    /**
     * Defines what to execute once the From is being processed
     *
     * @param array $data Form data.
     * @return bool
     */
    protected function _execute(array $data)
    {
        return true;
    }
}
