<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Fv Entity
 *
 * @property int $id
 * @property string $fvNumber
 * @property \Cake\I18n\FrozenTime $created
 * @property string $value
 * @property string $company
 * @property string $address
 * @property string $email
 */
class Fv extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'fvNumber' => true,
        'created' => true,
        'value' => true,
        'company' => true,
        'address' => true,
        'email' => true
    ];
}
